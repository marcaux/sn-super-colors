# Super Colors

A quick toggle to colorize various elements with the Standard Notes Super Editor to help distinguishing between various formatting options. It should be available within the official plugins repository.

This Toggle is theme agnostic.

If you always want to use the latest version (I sometimes tend to adjust a few little things here and there), you can use my deployed version here:

https://marcaux.gitlab.io/sn-super-colors/ext.json

## Examples with existing themes

![Screenshot 1](https://marcaux.gitlab.io/sn-super-colors/screen1.png "Screenshot 1")

![Screenshot 2](https://marcaux.gitlab.io/sn-super-colors/screen2.png "Screenshot 2")

![Screenshot 3](https://marcaux.gitlab.io/sn-super-colors/screen3.png "Screenshot 3")

![Screenshot 4](https://marcaux.gitlab.io/sn-super-colors/screen4.png "Screenshot 4")
