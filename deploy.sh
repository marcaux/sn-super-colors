#!/bin/sh
apt update
apt install zip
zip -r sn-super-colors.zip ./ -x "package-lock.json" ".env" "*.sh" ".git*" ".git/*" "dist/.gitkeep" ".DS*" ".htaccess" "node_modules/*" "src/*" "*.txt"
mkdir public
mv sn-super-colors.zip public/
cd public
unzip sn-super-colors.zip
